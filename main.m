clear;
clc;
x1 = imread('1.jpg'); %name of the image in the current folder
im_crop = imcrop(x1, [251 351 255 255]); %resizing to 256x256 image 
image = im2uint8(im_crop); %to 8-bit
imgray = rgb2gray(image); %convertion to gray scale
s = size(imgray);
figure
imshow(imgray); title('original image 8 bit gray-scale')

for i = 1:s(1)
	for j = 1:s(2)
		x = imgray(i,j);
		if mod(x,2) == 1
			matrix{1}(i,j) = 1;
		end
		if mod(x/2,2) >= 1
			matrix{2}(i,j) = 1;
		end
		if mod(x/4,2) >= 1
			matrix{3}(i,j) = 1;
		end
		if mod(x/8,2) >= 1
			matrix{4}(i,j) = 1;
		end
		if mod(x/16,2) >= 1
			matrix{5}(i,j) = 1;
		end
		if mod(x/32,2) >= 1
			matrix{6}(i,j) = 1;
		end
		if mod(x/64,2) >= 1
			matrix{7}(i,j) = 1;
		end
		if x/128 >= 1
			matrix{8}(i,j) = 1;
		end
	end % this loop performs 8 bit slicing
end
figure
for i = 1:8 %plotting bit planes
	subplot(2,4,i)
	imshow(matrix{i})
	title([num2str(i),' bit slice plane'])
end
%% task 2 MSB pictures
for i = 1:s(1)
	for j = 1:s(2)
		matrix_3msb(i,j) = matrix{6}(i,j)*2^5 + matrix{7}(i,j)*2^6 + matrix{8}(i,j)*2^7;
		matrix_2msb(i,j) = matrix{7}(i,j)*2^6 + matrix{8}(i,j)*2^7;
	end
end
figure
subplot(1,3,1)
imshow(imgray)
title('original image')
subplot(1,3,2)
imshow(matrix_2msb/255)
title('image 2 MSB')
subplot(1,3,3)
imshow(matrix_3msb/255)
title('image 3 MSB')
%% task 3 zero padding image
a = mask_function(5); %input N for NxN mask matrix it fills witn 1,2,3... etc.
a(1) = 2; % values could be changed directly if needed

msk = size(a);

padding_matrix = zeros(s(1) + msk(1) - 1, s(2) + msk(2) - 1); % here is adding 2 for 3x3 matrix, if matrix is 5x5 change addition to 4 etc.
for i = 1:s(1)
	for j = 1:s(2)
		padding_matrix(i + (msk(1)-1)/2,j + (msk(2)-1)/2) = imgray(i,j);
	end
end
imshow(padding_matrix/255) %original image with padding
title('original image with px padding')
%% task 4 Sobel edge detection
%in this case we need exact matrix so we can declare it (Sobel)
mask = [-1,0,1; -2,0,2; -1,0,1]; %Sobel vertical edge detector
mask_vertical_matrix_sobel = mask_filtering(padding_matrix, mask);
figure
subplot(1,3,1)
%mask_vertical_matrix_sobel = mask_vertical_matrix_sobel > 80;
imshow(mask_vertical_matrix_sobel/255)
title('sobel vertical edge filter')

mask_horizont = [1,2,1; 0,0,0; -1,-2,-1];
mask_horizontal_matrix_sobel = mask_filtering(padding_matrix, mask_horizont);
subplot(1,3,2)
imshow(mask_horizontal_matrix_sobel/255)
title('sobel gorizontal edge filter')

mixed_sobel_edge = sqrt(mask_horizontal_matrix_sobel.^2 + mask_vertical_matrix_sobel.^2);
mixed_sobel_edge = mixed_sobel_edge > 160; %setting threshold
subplot(1,3,3)
imshow(mixed_sobel_edge)
title('mixed & thresholded sobel edge detection');

%% task 5 laplacian mask
%upd before changing size of mask, make an appropriate change in previous
%section to create correct padding_matrix.
laplacian_mask = [-1,-1,-1; -1 ,8,-1;-1,-1,-1]; %laplacian filter mask
laplacian_matrix = mask_filtering(padding_matrix, laplacian_mask);
figure
imshow(laplacian_matrix/255)
title('laplacian transform mask')
%% task 6 Hough transform
m = mixed_sobel_edge;
sze = size(m);
p_max = round(sqrt(sze(1)^2 + sze(2)^2));
theta = -90:1:89;
rho = -p_max:1:p_max;

H = zeros(length(rho), length(theta));


for i = 1:sze(1)
	for j = 1:sze(2)
		if m(i,j) > 0
			for im_tet = 1:length(theta)
				t = theta(im_tet)*pi/180; %degrees
				dist = i*cos(t) + j*sin(t);
				[d, im_rho] = min(abs(rho - dist)); %d is minimum value(d < 1), im_rho is location of the minimum value
				H(im_rho, im_tet) = H(im_rho,  im_tet) + 1; %accumulates the total intensity
			end
		end
	end
end
figure
imshow(H/75) %75 is random number, just to increase the contrast
% another option was to insert counter for votes and then divide output by
% max number of votes
title('Hough transform')